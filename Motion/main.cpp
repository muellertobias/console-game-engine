#include <chrono>
#include <string>
#include "App/Motion.h"

using namespace std;

int main()
{
    int pixelSize = 4;
    int height = 480 / pixelSize;
    int width = 360 / pixelSize;

    Motion motion;
    motion.SetUp(height, width, pixelSize, pixelSize);
    motion.Run();

    return 0;
}