#ifndef PLAYER_H
#define PLAYER_H

#include "GraphicEngine.h"
#include "Common.h"
#include "Shapes/Shape.h"
#include "Shapes/Rectangle.h"
#include "World.h"

using namespace ConsoleGameEngine::Shapes;
using namespace ConsoleGameEngine::DataTypes;
using namespace ConsoleGameEngine;


typedef struct {
	KEY_CODE key;
	Axis axis;
} ButtonMapping;

class Player 
{
public:
	Player(Vec2d startPosition, World* w)
		: hitbox(startPosition, 5, 5, PIXEL_TYPE::SOLID, PIXEL_COLOR::BLUE, PIXEL_COLOR::BLUE)
	{
		this->world = w;
	}

	void ReadInputs(InputController& controller, seconds elapsedTime);
	ConsoleGameEngine::Shapes::Rectangle* GetHitbox();

private:
	World* world;

	ConsoleGameEngine::Shapes::Rectangle hitbox;
	const Vec2d_generic<float> Accelaration = { 700, 700 };
	const Vec2d_generic<float> MaxVelocity = { 400, 400 };
	const ButtonMapping ButtonMapping[4] =
	{
		{ KEY_CODE::W, Axis::Y },
		{ KEY_CODE::S, Axis::Y },
		{ KEY_CODE::A, Axis::X },
		{ KEY_CODE::D, Axis::X }
	};

	Vec2d_generic<float> Velocity = { 0, 0 };

	void MoveForward(ConsoleGameEngine::Shapes::Rectangle hitbox, float elapsedTime, float accelaration, float& velocity, float& changeInPosition);
	void MoveBackwards(ConsoleGameEngine::Shapes::Rectangle hitbox, float elapsedTime, float accelaration, float& velocity, float& changeInPosition);


};

#endif // PLAYER_H