#include "Player.h"

ConsoleGameEngine::Shapes::Rectangle* Player::GetHitbox()
{
	return &this->hitbox;
}

void Player::MoveForward(ConsoleGameEngine::Shapes::Rectangle hitbox, float elapsedTime, float accelaration, float& currentVelocity, float& changeInPosition)
{
	float velocity = currentVelocity;
	float deltaPosition = changeInPosition;

	velocity = max(velocity, 0);
	velocity += accelaration * elapsedTime;
	velocity = min(velocity, this->MaxVelocity[Axis::Y]);
	deltaPosition += velocity * elapsedTime;

	hitbox.Move({ 0, (int)deltaPosition });
	if (world->IsColliding(&hitbox))
	{
		currentVelocity = 0;
		changeInPosition = 0;
	}
	else
	{
		changeInPosition = deltaPosition;
		currentVelocity = velocity;
	}
}

void Player::MoveBackwards(ConsoleGameEngine::Shapes::Rectangle hitbox, float elapsedTime, float accelaration, float& currentVelocity, float& changeInPosition)
{
	float velocity = currentVelocity;
	float deltaPosition = changeInPosition;

	velocity = min(velocity, 0);
	velocity -= accelaration * elapsedTime;
	velocity = max(velocity, -this->MaxVelocity[Axis::Y]);
	deltaPosition += velocity * elapsedTime;

	hitbox.Move({ 0, (int)deltaPosition });
	if (world->IsColliding(&hitbox))
	{
		changeInPosition = 0;
		currentVelocity = 0;
	}
	else
	{
		changeInPosition = deltaPosition;
		currentVelocity = velocity;
	}
}

void Player::ReadInputs(InputController& controller, seconds elapsedTime)
{
	ConsoleGameEngine::Shapes::Rectangle rect = hitbox;
	Vec2d_generic<float> changeInPosition;
	float deltaPosition = 0.0f;
	float velocity = 0.0f;

	if (controller.GetInput(KEY_CODE::W).CurrentState == KEY_STATE::PRESSED || controller.GetInput(KEY_CODE::W).CurrentState == KEY_STATE::HOLD)
	{
		MoveBackwards(hitbox, elapsedTime, this->Accelaration[Axis::Y], this->Velocity[Axis::Y], changeInPosition[Axis::Y]);
	}
	else if(controller.GetInput(KEY_CODE::W).CurrentState == KEY_STATE::RELEASED)
	{
		changeInPosition[Axis::Y] = 0;
		this->Velocity[Axis::Y] = 0;
	}

	if (controller.GetInput(KEY_CODE::A).CurrentState == KEY_STATE::PRESSED || controller.GetInput(KEY_CODE::A).CurrentState == KEY_STATE::HOLD)
	{
		MoveBackwards(hitbox, elapsedTime, this->Accelaration[Axis::X], this->Velocity[Axis::X], changeInPosition[Axis::X]);
	}
	else if (controller.GetInput(KEY_CODE::A).CurrentState == KEY_STATE::RELEASED)
	{
		changeInPosition[Axis::X] = 0;
		this->Velocity[Axis::X] = 0;
	}

	if (controller.GetInput(KEY_CODE::S).CurrentState == KEY_STATE::PRESSED || controller.GetInput(KEY_CODE::S).CurrentState == KEY_STATE::HOLD)
	{
		MoveForward(hitbox, elapsedTime, this->Accelaration[Axis::Y], this->Velocity[Axis::Y], changeInPosition[Axis::Y]);
	}
	else if (controller.GetInput(KEY_CODE::S).CurrentState == KEY_STATE::RELEASED)
	{
		changeInPosition[Axis::Y] = 0;
		this->Velocity[Axis::Y] = 0;
	}

	if (controller.GetInput(KEY_CODE::D).CurrentState == KEY_STATE::PRESSED || controller.GetInput(KEY_CODE::D).CurrentState == KEY_STATE::HOLD)
	{
		MoveForward(hitbox, elapsedTime, this->Accelaration[Axis::X], this->Velocity[Axis::X], changeInPosition[Axis::X]);
	}
	else if (controller.GetInput(KEY_CODE::D).CurrentState == KEY_STATE::RELEASED)
	{
		changeInPosition[Axis::X] = 0;
		this->Velocity[Axis::X] = 0;
	}

	if (controller.GetInput(KEY_CODE::Q).CurrentState == KEY_STATE::PRESSED)
	{
		this->hitbox.SetOrigin({ 0, 0 });
	}

	this->hitbox.Move(Vec2d((int)changeInPosition.X(), (int)changeInPosition.Y()));
}

