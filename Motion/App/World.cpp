#include "World.h"

bool World::IsColliding(ConsoleGameEngine::Shapes::Rectangle* hitbox)
{
	for (auto const& s : objects) 
	{
		if (s->IsColliding(hitbox))
			return true;
		
		// cast shape 
		// calculate collisition
	}

	return false;
}

void World::PlaceObject(Shape* object)
{
	this->objects.push_back(object);
}

void World::Render(GraphicPlotter& plotter)
{
	for (auto const& s : objects)
	{
		s->Render(plotter);
	}
	hitbox->Render(plotter);
}
