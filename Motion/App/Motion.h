#pragma once

#include "GraphicEngine.h"
#include "Common.h"
#include "Shapes/Shape.h"
#include "Shapes/Rectangle.h"
#include "Player.h"
#include "World.h"

using namespace ConsoleGameEngine::Shapes;
using namespace ConsoleGameEngine::DataTypes;
using namespace ConsoleGameEngine;

class Motion : public GraphicEngine
{
public:
	Motion() : GraphicEngine() { }

protected:
	virtual void OnCreate() override;
	virtual void OnUpdate(seconds elapsedTime) override;

private:
	World* world;
	Player* player;
};

