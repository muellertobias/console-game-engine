#ifndef WORLD_H
#define WORLD_H

#include "GraphicEngine.h"
#include "Common.h"
#include "Shapes/Shape.h"
#include "Shapes/Rectangle.h"
#include <list>

using namespace ConsoleGameEngine::Shapes;
using namespace ConsoleGameEngine::DataTypes;
using namespace ConsoleGameEngine;

class World : public ConsoleGameEngine::Shapes::Rectangle
{
public:
	World(uint32_t height, uint32_t width) :
		ConsoleGameEngine::Shapes::Rectangle({ 0,0 }, height, width, PIXEL_TYPE::SOLID, PIXEL_COLOR::BLACK, PIXEL_COLOR::BLACK) {}

	bool IsColliding(ConsoleGameEngine::Shapes::Rectangle* hitbox);

	void SetPlayer(ConsoleGameEngine::Shapes::Rectangle* hitbox) 
	{
		this->hitbox = hitbox;
	}
	void PlaceObject(Shape* object);

	virtual void Render(GraphicPlotter& plotter) override;

private:
	ConsoleGameEngine::Shapes::Rectangle* hitbox;
	std::list<Shape*> objects;
};

#endif // WORLD_H