#include "Motion.h"

void Motion::OnCreate()
{
	this->appName = L"Motion Demo";
	this->world = new World(this->GetScreenHeight(), this->GetScreenWidth());
	this->player = new Player({30, 30 }, this->world);
	world->PlaceObject(new ConsoleGameEngine::Shapes::Rectangle({ 40, 40 }, 10, 30, PIXEL_TYPE::SOLID, PIXEL_COLOR::YELLOW, PIXEL_COLOR::YELLOW));
	world->SetPlayer(player->GetHitbox());
}

void Motion::OnUpdate(seconds elapsedTime)
{
	player->ReadInputs(*this, elapsedTime);
	world->Render(*this);
}
