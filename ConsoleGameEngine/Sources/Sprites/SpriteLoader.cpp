#include "SpriteLoader.h"
#include <vector>
#include <lodepng.h>

using namespace ConsoleGameEngine::Sprites;

Sprite* SpriteLoader::Load(const char* filename)
{
	uint32_t weight;
	uint32_t height;
	std::vector<uint8_t> buffer;
	std::vector<uint8_t> image;

	lodepng::load_file(buffer, filename);
	uint32_t error = lodepng::decode(image, weight, height, buffer);

	if (error)
	{
		// TODO: Improve error handling
		return GenerateErrorSprite();
	}
	else
	{
		return new Sprite(image, { weight, height });
	}
}

Sprite* SpriteLoader::GenerateErrorSprite()
{
	std::vector<uint8_t> picture(
		{
			W,R,R,R,W,
			R,R,W,W,R,
			R,W,R,W,R,
			R,W,W,R,R,
			R,R,R,R,W
		});
	std::vector<uint8_t> image;
	image.resize(picture.size() * 4);

	for (size_t i = 0; i < picture.size(); i++) 
	{
		image[i * 4 + 0] = 0;
		image[i * 4 + 1] = 1;
		image[i * 4 + 2] = 1;
		image[i * 4 + 3] = 2;
	}

	return new Sprite(image, { 5, 5 });
}
