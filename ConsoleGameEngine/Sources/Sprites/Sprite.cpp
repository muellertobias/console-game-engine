#include "Sprite.h"
#include <lodepng.h>
#include <iostream>

using namespace ConsoleGameEngine::Sprites;

void Sprite::Render(GraphicPlotter& plotter)
{
	int w = size.X();
	int h = size.Y();
	int jump = 1;
	for (unsigned y = 0; y + jump - 1 < h; y += jump)
		for (unsigned x = 0; x + jump - 1 < w; x += jump) {
			//get RGBA components
			uint32_t r = image[4 * y * w + 4 * x + 0]; //red
			uint32_t g = image[4 * y * w + 4 * x + 1]; //green
			uint32_t b = image[4 * y * w + 4 * x + 2]; //blue
			uint32_t a = image[4 * y * w + 4 * x + 3]; //alpha

			//make translucency visible by placing checkerboard pattern behind image
			int checkerColor = 191 + 64 * (((x / 16) % 2) == ((y / 16) % 2));
			r = (a * r + (UINT8_MAX - a) * checkerColor) / UINT8_MAX;
			g = (a * g + (UINT8_MAX - a) * checkerColor) / UINT8_MAX;
			b = (a * b + (UINT8_MAX - a) * checkerColor) / UINT8_MAX;

			if (r + g + b < 3 * UINT8_MAX)
			{
				plotter.Draw({ (int)x, (int)y }, PIXEL_TYPE::SOLID, ColorHelper::FromRGB(r, g, b));
			}
		}
}
