#ifndef SPRITELOADER_H
#define SPRITELOADER_H

#include "../Common.h"
#include "Sprite.h"

namespace ConsoleGameEngine
{
	namespace Sprites 
	{
#define R (uint8_t)PIXEL_COLOR::RED
#define W (uint8_t)PIXEL_COLOR::WHITE

		class SpriteLoader
		{
		public:
			static Sprite* Load(const char* filename);

		private:
			SpriteLoader() { }
			static Sprite* GenerateErrorSprite();
		};
	};
	
};

#endif //SPRITELOADER_H