#pragma once
#include "../Common.h"
#include <vector>

using namespace ConsoleGameEngine;
using namespace ConsoleGameEngine::DataTypes;

namespace ConsoleGameEngine
{
	namespace Sprites
	{
		class Sprite : public IRenderable
		{
		public:
			Sprite(std::vector<uint8_t> image, Vec2d_ui32 size)
				: image(image), size(size) { };
			virtual void Render(GraphicPlotter& plotter) override;

		private:
			Vec2d_ui32 size;
			std::vector<uint8_t> image;
		};
	};
};
