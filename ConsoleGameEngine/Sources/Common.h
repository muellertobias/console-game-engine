#ifndef COMMON_H
#define COMMON_H

#include <cstdint>
#include "DataTypes/PixelColor.h"
#include "DataTypes/PixelType.h"
#include "DataTypes/Vec2d.h"
#include "DataTypes/KeyTypes.h"

namespace ConsoleGameEngine 
{
	using namespace ConsoleGameEngine::DataTypes;

	typedef DataTypes::Vec2d_generic<int32_t> Vec2d;
	typedef DataTypes::Vec2d_generic<uint32_t> Vec2d_ui32;

	typedef float seconds;
	typedef uint32_t milliseconds;

	enum class GAME_MENU : uint8_t
	{
		CONTINUE,
		MENU_ACTIVE,
		EXIT
	};

	class GraphicPlotter {
	public:
		virtual void Draw(int x, int y,
			PIXEL_TYPE type = PIXEL_TYPE::SOLID,
			PIXEL_COLOR color = PIXEL_COLOR::WHITE) = 0;

		virtual void Draw(const Vec2d& position,
			PIXEL_TYPE type = PIXEL_TYPE::SOLID,
			PIXEL_COLOR color = PIXEL_COLOR::WHITE) = 0;
	};

	class InputController {
	public:
		virtual const Key GetInput(KEY_CODE keyCode) const = 0;
	};

	class IRenderable {
	public:
		virtual void Render(GraphicPlotter& plotter) = 0;
	};

	class ColorHelper {
	public:
		static PIXEL_COLOR FromRGB(uint8_t r, uint8_t g, uint8_t b)
		{
			uint8_t index = ((r > 128) | (g > 128) | (b > 128)) ? 8 : 0; // Bright bit
			index |= (r > 64) ? 4 : 0; // Red bit
			index |= (g > 64) ? 2 : 0; // Green bit
			index |= (b > 64) ? 1 : 0; // Blue bit
			return (PIXEL_COLOR)(index | index << 4);
		}

	private:
		ColorHelper() {}
	};
	
};

#endif // COMMON_H