#ifndef PIXELTYPES_H
#define PIXELTYPES_H

namespace ConsoleGameEngine::DataTypes
{
	enum class PIXEL_TYPE : uint16_t
	{
		SOLID = 0x2588,

		// All other pixel types as SOLID does not support the combined colors.
		// For this types you need to use the foreground colors like FG_WHITE.
		THREEQUARTERS = 0x2593,
		HALF = 0x2592,
		QUARTER = 0x2591,
	};
};

#endif // PIXELTYPES_H