#include "Rectangle.h"

using namespace ConsoleGameEngine::Shapes;

Rectangle::Rectangle(Vec2d origin, uint16_t width, uint16_t height, PIXEL_TYPE type, PIXEL_COLOR foreground, PIXEL_COLOR background)
{
    this->origin = origin;
    this->width = width;
    this->height = height;
    this->type = type;
    this->foreground = foreground;
    this->background = background;
}

void Rectangle::Render(GraphicPlotter& plotter)
{
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            if (y == 0 || y == height - 1 ||
                x == 0 || x == width - 1)
            {
                plotter.Draw(Vec2d(x, y) + origin, type, foreground);
            }
            else
            {
                plotter.Draw(Vec2d(x, y) + origin, type, background);
            }
        }
    }
}

void Rectangle::Move(Vec2d changeInPosition)
{
    this->origin[Axis::X] += changeInPosition[Axis::X];
    this->origin[Axis::Y] += changeInPosition[Axis::Y];
}

bool Rectangle::IsColliding(Shape* shape) const
{
    auto rect = dynamic_cast<Rectangle*>(shape);

    if (rect != nullptr &&
        this->origin.X() < rect->origin.X() + rect->width &&
        this->origin.X() + this->width > rect->origin.X() &&
        this->origin.Y() < rect->origin.Y() + rect->height &&
        this->origin.Y() + this->height > rect->origin.Y())
    {
        return true;
    }
    return false;
}

