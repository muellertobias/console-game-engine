#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Shape.h"

using namespace ConsoleGameEngine::DataTypes;

namespace ConsoleGameEngine 
{
	namespace Shapes
	{
		class Rectangle : public Shape 
		{
		public:
			Rectangle(Vec2d origin, uint16_t width, uint16_t height,
				PIXEL_TYPE type, PIXEL_COLOR foreground, PIXEL_COLOR background);

			virtual void Render(GraphicPlotter& plotter) override;
			virtual void Move(Vec2d changeInPosition) override;
			virtual bool IsColliding(Shape* shape) const override;

		private:
			uint16_t width;
			uint16_t height;

			PIXEL_TYPE type; 
			PIXEL_COLOR foreground;
			PIXEL_COLOR background;
		};
	}
}

#endif // RECTANGLE_H