#ifndef SHAPE_H
#define SHAPE_H

#include "../Common.h"
#include "../DataTypes/PixelType.h"

using namespace ConsoleGameEngine::DataTypes;

namespace ConsoleGameEngine 
{
	namespace Shapes
	{
		class Shape : public IRenderable
		{
		public:
			virtual void Render(GraphicPlotter& plotter) = 0;
			virtual void Move(Vec2d changeInPosition) = 0;
			virtual bool IsColliding(Shape* shape) const = 0;

			virtual void SetOrigin(Vec2d origin)
			{
				this->origin = origin;
			}

		protected:
			Vec2d origin;

		};
	}
}

#endif // SHAPE_H