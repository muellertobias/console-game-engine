#pragma once

#include <GraphicEngine.h>
#include <Common.h>
#include <Sprites/Sprite.h>
#include <Shapes/Rectangle.h>

using namespace ConsoleGameEngine;
using namespace ConsoleGameEngine::DataTypes;
using namespace ConsoleGameEngine::Sprites;

class SpritesApp : public GraphicEngine
{
public:
	SpritesApp() : GraphicEngine() { }

protected:
	// Inherited via GraphicEngine
	virtual void OnCreate() override;
	virtual void OnUpdate(seconds elapsedTime) override;

private:
	ConsoleGameEngine::Shapes::Rectangle* world;
	Sprite* sprite;
	Sprite* error;
};

