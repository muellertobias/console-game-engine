#include "SpritesApp.h"
#include "Sprites/SpriteLoader.h"

void SpritesApp::OnCreate()
{
	sprite = SpriteLoader::Load("D:\\Development\\console-game-engine\\Sprites\\smallknight.png");
	error = SpriteLoader::Load("D:\\Development\\console-game-engine\\Sprites\\thisisnotexisting.png");
	world = new Shapes::Rectangle({ 0, 0 }, this->GetScreenWidth(), this->GetScreenHeight(), PIXEL_TYPE::SOLID, PIXEL_COLOR::GREEN, PIXEL_COLOR::GREEN);
}

void SpritesApp::OnUpdate(seconds elapsedTime)
{
	world->Render(*this);
	sprite->Render(*this);
	error->Render(*this);
}
