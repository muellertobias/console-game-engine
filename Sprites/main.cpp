#include <chrono>
#include <string>
#include "App/SpritesApp.h"

using namespace std;

int main()
{
    int pixelSize = 8;
    int height = 480 / pixelSize;
    int width = 360 / pixelSize;

    SpritesApp app;
    app.SetUp(height, width, pixelSize, pixelSize);
    app.Run();

    return 0;
}